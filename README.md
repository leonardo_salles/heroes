# Heroes App

### Tecnologias empregadas na aplicação.

* [Data-Binding](https://developer.android.com/topic/libraries/data-binding/index.html?hl=pt-br) para binding de classes e atributos
* [Retrofit(Em inglês)](http://square.github.io/retrofit/) para requisições HTTP
* [Picasso(Em inglês)](http://square.github.io/picasso/) para requisições a imagens vindas de uma URL/URI
* Build com [Gradle(Em inglês)](https://gradle.org/)

### Como rodar

###### Opção 1:
Importar o projeto no Android Studio

Rodar em um device ou Emulador

###### Opção 2:
Gerar APK pelo Android Studio [Tutorial(Em inglês)](https://stackoverflow.com/questions/16709848/build-unsigned-apk-file-with-android-studio)