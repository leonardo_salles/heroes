package com.ilegra.heroes.interceptor;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ilegra.heroes.models.ApiResponse;
import com.ilegra.heroes.util.Api;

import org.json.JSONArray;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by leonardosalles on 29/06/17.
 */
public class ApiInterceptor implements Interceptor {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final Gson GSON = new Gson();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        // Auth params
        HashMap<String, String> auth = Api.getAuth();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("ts", auth.get("ts"))
                .addQueryParameter("apikey", auth.get("apikey"))
                .addQueryParameter("hash", auth.get("hash"))
                .build();

        Request.Builder requestBuilder = original.newBuilder()
                .url(url);

        Request request = requestBuilder.build();

        Response response = chain.proceed(request);
        final ResponseBody body = response.body();
        ApiResponse apiResponse = null;
        try {
            apiResponse = GSON.fromJson(body.string(), ApiResponse.class);
        } catch (NumberFormatException e) {
            apiResponse = new ApiResponse();
        }
        body.close();

        JsonArray listResults = apiResponse.getData().getAsJsonArray("results");
        int offset = apiResponse.getData().get("offset").getAsInt();
        int limit = apiResponse.getData().get("limit").getAsInt();
        int total = apiResponse.getData().get("total").getAsInt();

        Api.paginationOffset = offset > 0 ? offset : limit;
        Api.paginationTotal = total;
        Api.paginationLimit = limit;

        final Response.Builder newResponse = response.newBuilder()
                .body(ResponseBody.create(JSON, listResults.size() == 1 ? listResults.get(0).toString() : listResults.toString()));
        return newResponse.build();
    }
}