package com.ilegra.heroes;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.ilegra.heroes.databinding.MainBinding;
import com.ilegra.heroes.models.Hero;
import com.ilegra.heroes.models.HeroList;
import com.ilegra.heroes.services.MarvelService;
import com.ilegra.heroes.util.Api;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ListView heroesListView;
    private HeroList heroList;
    private int loadedItems = 0;
    private Button btnShowMore;
    private LinearLayout progressLoader;
    private List<Hero> listHeroesAll;
    Parcelable state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final MainBinding binding = DataBindingUtil.setContentView(this, R.layout.main);

        heroesListView = (ListView) findViewById(R.id.heroesListView);

        btnShowMore = new Button(this);
        btnShowMore.setText("Carregar mais...");

        btnShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int offset = Api.paginationOffset;
                offset += Api.paginationOffset;

                getItems(offset);
            }
        });

        btnShowMore.setVisibility(View.INVISIBLE);

        heroesListView.addFooterView(btnShowMore);
        heroesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("heroId", listHeroesAll.get(position).getId());
                startActivity(intent);
            }
        });

        heroList = new HeroList();
        binding.setHeroes(heroList);

        progressLoader = (LinearLayout) findViewById(R.id.progressLoader);
        progressLoader.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0));

        getItems(0);
    }

    private void getItems (int offset) {
        progressLoader.setVisibility(View.VISIBLE);
        progressLoader.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 300));

        MarvelService service = Api.getRetrofitBuilder().create(MarvelService.class);

        Call<List<Hero>> heroesCall = service.listHeroes(offset);

        heroesCall.enqueue(new Callback<List<Hero>>() {
            @Override
            public void onResponse(Call<List<Hero>> call, Response<List<Hero>> response) {
                List<Hero> listHeroes = response.body();

                for (int i=0,n=listHeroes.size(); i<n; i++) {
                    heroList.list.add(listHeroes.get(i));
                }

                listHeroesAll = heroList.list;

                // atualiza os itens carregados
                loadedItems = heroList.list.size();

                if (btnShowMore.getVisibility() == View.INVISIBLE) {
                    btnShowMore.setVisibility(View.VISIBLE);
                }

                progressLoader.setVisibility(View.INVISIBLE);
                progressLoader.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0));

                // REMOVE O BOTAO DE CARREGAR MAIS CASO JA TENHA CARREGADO TODOS
                if (loadedItems == Api.paginationTotal) {
                    heroesListView.removeFooterView(btnShowMore);
                }

                state = heroesListView.onSaveInstanceState();

                // PRESERVA O SCROLL DA LISTA
                heroesListView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (loadedItems > Api.paginationLimit) {
                            heroesListView.onRestoreInstanceState(state);
                            heroesListView.smoothScrollToPosition(50);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Hero>> call, Throwable t) {
                System.out.print(t);
            }
        });
    }

}
