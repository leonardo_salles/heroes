package com.ilegra.heroes.services;

import android.databinding.ObservableList;

import com.ilegra.heroes.models.Hero;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by leonardosalles on 29/06/17.
 */
public interface MarvelService {
    @GET("characters")
    Call<List<Hero>> listHeroes(@Query("offset") int offset);

    @GET("characters/{characterId}")
    Call<Hero> findHeroById(@Path("characterId") Long characterId);
}