package com.ilegra.heroes.binders;

import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.widget.ImageView;
import android.widget.ListView;

import com.ilegra.heroes.R;
import com.ilegra.heroes.adapters.ListAdapter;
import com.ilegra.heroes.models.Hero;
import com.ilegra.heroes.models.Thumbnail;
import com.squareup.picasso.Picasso;

/**
 * Created by leonardosalles on 29/06/17.
 */

public class ListBinder {

    @BindingAdapter({"bind:thumbnail"})
    public static void loadImage(ImageView view, Thumbnail thumbnail) {
        if (thumbnail != null) {
            Picasso.with(view.getContext())
                    .load(thumbnail.getPath() + "." + thumbnail.getExtension())
                    .placeholder(R.drawable.progress_animation)
                    .into(view);
        }
    }

    @BindingAdapter("bind:items")
    public static void bindList(ListView view, ObservableArrayList<Hero> list) {
        ListAdapter adapter = new ListAdapter(list);
        view.setAdapter(adapter);
    }
}
