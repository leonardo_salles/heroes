package com.ilegra.heroes.util;

import com.ilegra.heroes.interceptor.ApiInterceptor;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by leonardosalles on 29/06/17.
 */

public class Api {
    public static String API_PUBLIC_KEY = "14f545ea283b40b9ad29353a11f0a14a";
    public static String API_PRIVATE_KEY = "4b1549cbe378056254f5c78924718f710871fe55";
    public static String API_URL = "http://gateway.marvel.com/v1/public/";
    public static int paginationOffset = 0;
    public static int paginationTotal = 0;
    public static int paginationLimit = 0;

    public static Retrofit getRetrofitBuilder () {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new ApiInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }


    public static HashMap<String, String> getAuth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 5);

        String timestamp = String.valueOf(calendar.getTime().getTime());
        String hash = generateMd5(timestamp + API_PRIVATE_KEY + API_PUBLIC_KEY);

        HashMap<String, String> authOptions = new HashMap<String, String>();
        authOptions.put("ts", timestamp);
        authOptions.put("apikey", API_PUBLIC_KEY);
        authOptions.put("hash", hash);

        return authOptions;
    }

    // FONTE:
    // https://stackoverflow.com/questions/4846484/md5-hashing-in-android
    public static String generateMd5(String s) {
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(s.getBytes(),0,s.length());
        String hash = new BigInteger(1, m.digest()).toString(16);
        return hash;
    }
}
