package com.ilegra.heroes;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.ilegra.heroes.databinding.ContentDetailBinding;
import com.ilegra.heroes.models.Hero;
import com.ilegra.heroes.models.HeroList;
import com.ilegra.heroes.services.MarvelService;
import com.ilegra.heroes.util.Api;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailActivity extends AppCompatActivity {

    private LinearLayout progressLoader;
    private RelativeLayout relativeLayoutContent;
    private ContentDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        binding = DataBindingUtil.setContentView(this, R.layout.content_detail);
        relativeLayoutContent = (RelativeLayout) findViewById(R.id.relativeLayoutContent);

        Intent intent = getIntent();
        Long id = intent.getLongExtra("heroId", 0l);

        if (id > 0) {
            progressLoader = (LinearLayout) findViewById(R.id.progressLoader);
            progressLoader.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0));

            getCharacterDetails(id);
        }
    }

    private void getCharacterDetails (Long id) {
        progressLoader.setVisibility(View.VISIBLE);
        progressLoader.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 300));

        MarvelService service = Api.getRetrofitBuilder().create(MarvelService.class);
        Call<Hero> heroCall = service.findHeroById(id);

        heroCall.enqueue(new Callback<Hero>() {
            @Override
            public void onResponse(Call<Hero> call, Response<Hero> response) {
                Hero hero = response.body();
                binding.setHero(hero);

                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                toolbar.setTitle(hero.getName());

                setSupportActionBar(toolbar);

                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);

                relativeLayoutContent.setVisibility(View.VISIBLE);

                progressLoader.setVisibility(View.INVISIBLE);
                progressLoader.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0));
            }

            @Override
            public void onFailure(Call<Hero> call, Throwable t) {
                System.out.print(t);
            }
        });
    }
}
