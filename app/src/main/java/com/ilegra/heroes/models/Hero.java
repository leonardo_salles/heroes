package com.ilegra.heroes.models;

import java.util.List;

/**
 * Created by leonardosalles on 29/06/17.
 */

public class Hero {
    private Long id;
    private String name;
    private String description;
    private Thumbnail thumbnail;
    private Comics comics;
    private Series series;
    private Stories stories;
    private String comicsAsString;
    private String seriesAsString;
    private String storiesAsString;

    public Hero () {

    }

    public Hero(Long id, String name, String description, Thumbnail thumbnail, Comics comics, Series series, Stories stories) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.thumbnail = thumbnail;
        this.comics = comics;
        this.series = series;
        this.stories = stories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description != null && description != "" ? description : "Nenhuma descrição econtrada...";
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Comics getComics() {
        return comics;
    }

    public void setComics(Comics comics) {
        this.comics = comics;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public Stories getStories() {
        return stories;
    }

    public void setStories(Stories stories) {
        this.stories = stories;
    }

    public String getComicsAsString() {
        String items =  "";
        List<Item> listItems = this.comics.getItems();
        for (int i=0,n=listItems.size(); i<n; i++) {
            items += listItems.get(i).getName();

            if (i < n -1) {
                items += "\n ";
            }
        }

        if (items == "") {
            items = "Nenhum quadrinho encontrado...";
        }

        return items;
    }

    public String getSeriesAsString() {
        String items =  "";
        List<Item> listItems = this.series.getItems();
        for (int i=0,n=listItems.size(); i<n; i++) {
            items += listItems.get(i).getName();

            if (i < n -1) {
                items += "\n ";
            }
        }

        if (items == "") {
            items = "Nenhuma série encontrada...";
        }

        return items;
    }

    public String getStoriesAsString() {
        String items =  "";
        List<Item> listItems = this.stories.getItems();
        for (int i=0,n=listItems.size(); i<n; i++) {
            items += listItems.get(i).getName();

            if (i < n -1) {
                items += "\n ";
            }
        }

        if (items == "") {
            items = "Nenhuma história encontrada...";
        }

        return items;
    }
}
