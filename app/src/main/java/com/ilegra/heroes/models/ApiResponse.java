package com.ilegra.heroes.models;

import com.google.gson.JsonObject;

/**
 * Created by leonardosalles on 29/06/17.
 */

public class ApiResponse {
    private String status;
    private int code;
    private JsonObject data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }
}