package com.ilegra.heroes.models;

/**
 * Created by leonardosalles on 29/06/17.
 */
import android.databinding.ObservableArrayList;
import android.view.View;

public class HeroList {
    public ObservableArrayList<Hero> list = new ObservableArrayList<>();

    private void add(Hero hero) {
        list.add(hero);
    }

}
