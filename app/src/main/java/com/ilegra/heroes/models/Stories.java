package com.ilegra.heroes.models;

import java.util.List;

/**
 * Created by leonardosalles on 30/06/17.
 */

/**
 * Created by leonardosalles on 29/06/17.
 */

public class Stories {
    private String available;
    private String collectionURI;
    private List<Item> items;

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
