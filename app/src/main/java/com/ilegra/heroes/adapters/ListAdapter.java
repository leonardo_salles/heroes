package com.ilegra.heroes.adapters;

/**
 * Created by leonardosalles on 29/06/17.
 */

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ilegra.heroes.R;
import com.ilegra.heroes.databinding.HeroItemBinding;
import com.ilegra.heroes.models.Hero;

public class ListAdapter extends BaseAdapter {
    private ObservableArrayList<Hero> list;
    private LayoutInflater inflater;

    public ListAdapter(ObservableArrayList<Hero> l) {
        list = l;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        HeroItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.hero_item, parent, false);
        binding.setHero(list.get(position));

        return binding.getRoot();
    }
}